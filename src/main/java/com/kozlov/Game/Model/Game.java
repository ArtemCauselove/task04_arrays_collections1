package com.kozlov.Game.Model;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;
public class Game {
    private static Logger logger1 = LogManager.getLogger(Game.class);
    public  void main() {
        int[] door = new int[10];
        Scanner scan = new Scanner(System.in);
        for (int i = 0; i < door.length; i++) {
            door[i] = scan.nextInt();
        }
        for (int i = 0; i < door.length; i+=2) {
            //String s = "[" + i + "]" + "door: " + door[i] + " ";
            logger1.info("\n" + "[" + (i+1) + "]" + "door:" + door[i] + "  [" + (i+2) + "]" + "door: " + door[i+1]);
            //System.out.print("[" + i + "]" + "door: " + door[i] + " ");
        }
        orderOfDoors(25, door);
        logger1.info(
                "\n     1   2   3   4\n" +
                "   ["+door[0]+ "] ["+door[1]+"] ["+door[2]+"] ["+door[3]+ "]\n" +
                "10 ["+door[9]+ "]   you   ["+door[4]+ "] 5\n" +
                "   ["+door[8]+ "] ["+door[7]+ "] ["+door[6]+ "] ["+door[5]+ "]\n" +
                "   9    8    7   6\n");
        dead(0, door);
        orderOfDoors(25, door);
    }
    public static void dead(int i, int[] door) {
        if ((25 + door[i]) < 0) {
            logger1.info(" \n" + "You will die if you open " + (i + 1) + " door");
        }
        i++;
        if (i < 10) {
            dead(i, door);
        }
    }
    public static void orderOfDoors(int strength, int[] door) {
        for (int i = 0; i < 10; i++) {
            if ((door[i] >= 10) && (door[i] <= 80)) {
                logger1.info(" \n" + "Open " + (i+1) + " door");
                strength = strength + door[i];
            }
        }
        for (int i = 0; i < 10; i++) {
            if (door[i] < 0) {
                if ((strength + door[i]) >= 0) {
                    strength = strength + door[i];
                    logger1.info(" \n" + "Open " + (i+1) + " door" +" \n");
                } else {
                    logger1.info("You could`nt have any chance " + " \n");
                    break;
                }
            }
        }
    }
}
