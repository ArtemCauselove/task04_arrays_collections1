package com.kozlov.Game.Controller;
import com.kozlov.Game.Model.Game;
import com.kozlov.Game.View.View;
public class Controller {
    public void start(){
        View view = new View();
        Game game = new Game();
        view.printStart();
        game.main();
    }
}
