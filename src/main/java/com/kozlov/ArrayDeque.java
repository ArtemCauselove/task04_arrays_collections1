package com.kozlov;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

    public class ArrayDeque<E> implements java.util.Deque<E> {

        private int leftIndex;
        private int rightIndex;

        private Object[] data;

        public ArrayDeque(int size) {
            leftIndex = 0;
            rightIndex = 10;
            data = new Object[size];
        }

        public ArrayDeque() {
            leftIndex = 0;
            rightIndex = 10;
            data = new Object[rightIndex];
        }

        private void expandToLeft() {
            Object[] arrayDataContainer = getCopyArray();
            leftIndex = calculateNewSize() - data.length;
            rightIndex += leftIndex;
            resize(arrayDataContainer);
        }

        private void expandToRight() {
            Object[] arrayDataContainer = getCopyArray();
            resize(arrayDataContainer);
        }

        private Object[] getCopyArray() {
            Object[] copy = new Object[rightIndex - leftIndex];
            for (int i = leftIndex, k = 0; i < rightIndex; i++, k++) {
                copy[k] = data[i];
            }
            return copy;
        }

        private void resize(final Object[] arrayContainer) {
            int newSize = calculateNewSize();
            data = new Object[newSize];
            transferData(arrayContainer);
        }

        private void transferData(final Object[] fromContainer) {
            for (int i = 0, k = leftIndex; i < fromContainer.length; i++, k++) {
                data[k] = fromContainer[i];
            }
        }

        private int calculateNewSize() {
            return (int) ((data.length + 1) * 1.5);
        }

        @Override
        public void addFirst(final E o) {
            if (leftIndex == 0) {
                expandToLeft();
            }
            data[leftIndex] = o;
            leftIndex--;
        }

        @Override
        public void addLast(final E o) {
            if (rightIndex == data.length) {
                expandToRight();
            }
            data[rightIndex] = o;
            rightIndex++;
        }

        @Override
        public boolean isEmpty() {
            return leftIndex == rightIndex;
        }

        @Override
        public Object[] toArray() {
            return data;
        }

        @Override
        public Object[] toArray(Object[] a) {
            return new Object[0];
        }

        @Override
        public boolean containsAll(Collection c) {
            return false;
        }

        @Override
        public boolean addAll(Collection<? extends E> c) {
            Object[] collection = c.toArray();
            for (int i = 0; i < c.size(); i++) {
                addLast((E) collection[i]);
            }
            return true;
        }

        @Override
        public boolean removeAll(Collection c) {
            return false;
        }

        @Override
        public boolean retainAll(Collection c) {
            return false;
        }

        @Override
        public void clear() {
            leftIndex = 0;
            rightIndex = 10;
            data = new Object[rightIndex];
        }

        @Override
        public boolean removeIf(Predicate filter) {
            return false;
        }

        @Override
        public Spliterator spliterator() {
            return null;
        }

        @Override
        public Stream stream() {
            return null;
        }

        @Override
        public Stream parallelStream() {
            return null;
        }

        @Override
        public void forEach(Consumer action) {

        }

        @Override
        public boolean offerFirst(Object o) {
            if (rightIndex == data.length) {
                return false;
            }
            data[rightIndex] = o;
            rightIndex++;
            return true;
        }

        @Override
        public boolean offerLast(Object o) {

            return false;
        }

        @Override
        public E removeFirst() {
            Object element = data[leftIndex + 1];
            if (element != null) {
                data[++leftIndex] = null;
            }
            return (E) element;
        }

        @Override
        public E removeLast() {
            Object element = data[rightIndex - 1];
            if (element != null) {
                data[--rightIndex] = null;
            }
            return (E) element;
        }

        @Override
        public E pollFirst() {
            if(isEmpty()==true){
                return null;
            }
            Object element = data[0];
            if (element != null) {
                data[0] = null;
            }
            return (E) element;

        }

        @Override
        public E pollLast() {
            if(isEmpty()==true){
                throw new NoSuchElementException();
            }
            return (E) data[leftIndex + 1];
        }

        @Override
        public E getFirst() {
            if(isEmpty()){
                throw new NullPointerException();
            }
            return (E) data[leftIndex + 1];
        }

        @Override
        public E getLast() {
            if(isEmpty()){
                throw new NullPointerException();
            }
            return (E) data[rightIndex - 1];
        }

        @Override
        public E peekFirst() {
            if(isEmpty()){
            return null;
            }
            return (E) data[leftIndex + 1];
        }

        @Override
        public E peekLast() {
            if (isEmpty()) {
            return null;
            }
            return (E) data[rightIndex - 1];
        }

        @Override
        public boolean removeFirstOccurrence(Object o) {
            return false;
        }

        @Override
        public boolean removeLastOccurrence(Object o) {
            return false;
        }

        @Override
        public boolean add(Object o) {
            return false;
        }

        @Override
        public boolean offer(Object o) {
            return false;
        }

        @Override
        public E remove() {
            return null;
        }

        @Override
        public E poll() {
            return null;
        }

        @Override
        public E element() {
            return null;
        }

        @Override
        public E peek() {
            return null;
        }

        @Override
        public void push(Object o) {

        }

        @Override
        public E pop() {
            return null;
        }

        @Override
        public boolean remove(Object o) {
            return false;
        }

        @Override
        public boolean contains(Object current) {
            for (int i = 0; i < data.length; i++) {
                if (data[i] == current) return true;
            }
            return false;
        }

        @Override
        public int size() {
            return rightIndex - leftIndex - 1;
        }

        @Override
        public Iterator iterator() {
            return null;
        }

        @Override
        public Iterator descendingIterator() {
            return null;
        }

        @Override
        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = leftIndex; i < rightIndex; i++) {
                if (data[i] != null) {
                    stringBuilder.append(data[i].toString()).append(" ");
                }
            }
            return stringBuilder.toString();
        }
    }

