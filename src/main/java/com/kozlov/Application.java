package com.kozlov;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

public class Application {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    public static void main(String[] args) {
        Deque<Integer> dequeExample = new ArrayDeque<>();
        List<Integer> array = new ArrayList<>();
        array.add(111);
        array.add(222);
        array.add(333);
        array.add(444);
        dequeExample.addLast(5);
        dequeExample.addFirst(7);
        dequeExample.addFirst(17);
        dequeExample.addLast(4);
        dequeExample.removeLast();
        dequeExample.addFirst(1);
        dequeExample.addLast(2);
        int retval = dequeExample.peekFirst();
        logger1.info("Result of peeking first  " + retval+"\n");
        logger1.info(dequeExample);
        logger1.info("\n" + dequeExample.contains(2));
        dequeExample.addAll(array);
        logger1.info("\n" + dequeExample);
    }
}
